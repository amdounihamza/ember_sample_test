import Route from '@ember/routing/route';

export default Route.extend({

    model() {
        return this.store.findAll('blog');
    },

    actions: {
        goToAdmin() {
            this.transitionTo('blog.admin');
        }
    }

});
