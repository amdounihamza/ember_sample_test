import Route from '@ember/routing/route';
import { get, set } from '@ember/object';

export default Route.extend({
    // name: null,
    // description: null,

     actions: {
        goBackToIndex() {
            this.transitionTo('blog.index');
        },

        toggleEdit() {
            this.controller.toggleProperty('editable');
        },
         
        createUser() {
          let formName = get(this, 'name');
          let formDescription = get(this, 'description');
          //console.log(formName, formDescription);
          let newRecord = this.store.createRecord('blog', {
            name: formName,
            description: formDescription
          })
          newRecord.save()
        },
        updateBlog() {
          let updatedName = this.get('blog.name');
          let updatedDate = get(this, 'blog.createdAt');
          let updatedDescription = get(this, 'blog.description')
          let blog = this.get('blog').findBy('id', 'blog.id')
          blog.set('name', updatedName);
          blog.set('createdAt', updatedDate);
          blog.set('updatedDescription', updatedDescription);
          blog.save();
        },
        destroyBlog() {
            let destroyId = this.get('destroyBlog')
            let blog = this.get('blog').findBy('id', destroyId)
            blog.destroyRecord();
          }
     }
});
