import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
    name: attr('string'),
    createdAt: attr('string'),
    avatar: attr('string'),
    description: attr('string')
});
