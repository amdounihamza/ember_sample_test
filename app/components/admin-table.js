import Component from '@ember/component';
import { get, set } from '@ember/object';



export default Component.extend({
     //blog: null,

    init() {
        this._super(...arguments);
        //console.log('from init');
    },

    didReceiveAttrs() {
        this._super(...arguments);
        console.log('didReceiveAttrs', get(this, 'blog'));
    },

    willRender() {
        this._super(...arguments);
    },

    didInsertElement() {
        this._super(...arguments);
    },

    didRender() {
        this._super(...arguments);
    }
});
