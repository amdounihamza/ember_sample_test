import DS from 'ember-data';

export default DS.RESTAdapter.extend({
    host: 'https://5cc85fb62dcd9d0014768e8b.mockapi.io',
    namespace: 'api/v1/',
    pathForType() {
        return 'blog'; // path for access to blog data https://5cc85fb62dcd9d0014768e8b.mockapi.io/api/v1/blog
    }

});
